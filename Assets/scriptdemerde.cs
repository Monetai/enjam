﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class scriptdemerde : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        bool mustLoad = false;
	    foreach( BasePlayer player in PlayerManager.Instance.Players)
        {
            if (player.playerInput.GetButtonDown(InputNames.BtnStart) && mustLoad == false)
            {
                mustLoad = true;
                
            }
        }

        if (mustLoad && IsInvoking() == false)
        {
            GetComponent<AudioSource>().Play();
            Transition.Instance.BeginTransition();
            Invoke("load", 3f);
        }
            
	}

    void load()
    {
        SceneManager.LoadScene("Lobby");
    }
}
