﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {

    public static Transition Instance;

    void Start()
    {
        Instance = this;
    }
	
	// Update is called once per frame
	public void BeginTransition ()
    {
        foreach (EMTransition trans in GetComponentsInChildren<EMTransition>())
            trans.Play();
	}
}
