﻿using UnityEngine;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

	public static SoundManager Instance;

	public AudioSource src;

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayClip(AudioSource source, AudioClip clip, float volume=1f, float pitch=1f)
	{
		if (!source) source = src;

		source.pitch = pitch;
		source.PlayOneShot(clip,volume);
	}
}
