﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameRules : MonoBehaviour {

	public enum State
	{
		Undefined,
		BattleOngoing,
		BattleFinished
	}

	public State currentState= State.BattleOngoing;

	public static GameRules Instance;
	[Range(0, 100)]
	public int maxScore = 10;

    [Range(0, 5 * 60)]
    public float countDownDuration = 3*60;
	private float remainingTime;
	public float RemainingTime { get { return remainingTime; } }

	void Start() {
		DontDestroyOnLoad(gameObject);
	}
	// Use this for initialization
	void Awake () {
		Instance = this;
		remainingTime = countDownDuration;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (currentState == State.BattleOngoing) {

			if (!ManageTime(Time.deltaTime)) {
				currentState = State.BattleFinished;
			}
			
		} else if (currentState == State.BattleFinished){
            SceneManager.LoadScene("Score");
            currentState = State.Undefined;
        }

	}

	void StartRound()
	{
		remainingTime = countDownDuration;
		currentState = State.BattleOngoing;
	}

	bool ManageTime(float deltaTime)
	{

		if ( remainingTime > 0 ) {
			remainingTime -= deltaTime;
			return true;
		} else {
			remainingTime = 0;
			return false;
		}
	}

}
