﻿using UnityEngine;
using System.Collections;

public class SFXManager : MonoBehaviour
{
    public SoundBank FootSteps;
    public SoundBank HurtSounds;
    public SoundBank DashSound;
    public SoundBank JumpSound;
    public SoundBank GetFlowerSound;
    public SoundBank CombiHornSound;
    public SoundBank CutSound;

    public void PlayFootStep()
    {
        FootSteps.PlayRandomClip();
    }

    public void PlayHurtSounds()
    {
        HurtSounds.PlayRandomClip();
    }

    public void PlayDashSound()
    {
        DashSound.PlayRandomClip();
    }

    public void PlayJumpSound()
    {
        JumpSound.PlayRandomClip();
    }

    public void PlayGetFlowerSound()
    {
        GetFlowerSound.PlayRandomClip();
    }

    public void PlayCombiHornSound()
    {
        CombiHornSound.PlayRandomClip();
    }

    public void PlayCutSound()
    {
        CutSound.PlayRandomClip();
    }
}

