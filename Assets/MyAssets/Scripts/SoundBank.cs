﻿using UnityEngine;
using System.Collections.Generic;

public class SoundBank : MonoBehaviour {

	public List<AudioClip> soundBank = new List<AudioClip>();

	[Range(0, 2)]
	public float minRandomVolume = 1f;
	[Range(0,2)]
	public float maxRandomVolume=1f;
	[Range(0, 2)]
	public float minRandomPitch=0.85f;
	[Range(0,2)]
	public float maxRandomPitch = 1.15f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayClip(AudioClip clip, float volume=1f, float pitch=1f)
	{
		SoundManager.Instance.PlayClip(null,clip,volume,pitch);
	}

	public void PlayRandomClip()
	{
		if (soundBank.Count == 0) return;

		PlayClip(soundBank[Random.Range(0, soundBank.Count)],
			Random.Range(minRandomVolume, maxRandomVolume),
			Random.Range(minRandomPitch, maxRandomPitch));
	}
}
