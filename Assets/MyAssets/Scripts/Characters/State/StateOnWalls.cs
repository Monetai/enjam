﻿using UnityEngine;
using System.Collections;
using System;

//todo
//lancer un raycast depuis le haut du collider pour verifier si 
//la tete de polites touche un mur, si c'est pas le cas il faut 
//sortir de l'etat wallJump

public class StateOnWalls : State
{
    //sliding
    public float slideMaxSpeed = 200f;
    public Vector2 jumpDir;
    public float timeBeforeSlide = 0.3f;

    //axis for looking
    float horizontalAxis = 0;
    bool looking = false;

    bool exitWall = false;

    //timer for before gravity kick in
    float gravCounter = 0;
    float slideCounter;
    bool wantJump = false;

    BasePlayer playerRef;
    GlobalData playerData;

    protected override void Start()
    {
        type = StatesManager.statesType.STATE_ONWALLS;
        base.Start();

        playerRef = GetComponentInParent<Character>().player;
        playerData = GetComponentInParent<GlobalData>();
    }

    protected override void VerifyExitState()
    {
        if(physCtrl.grounded)
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_GROUNDED);
            return;
        }

        if (!physCtrl.grounded && !physCtrl.touchingWall)
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_INAIR);
            physCtrl.RestoreGravity();
            return;
        }

        if(exitWall)
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_INAIR);
            physCtrl.RestoreGravity();
            return;
        }

        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction5) || playerRef.playerInput.GetButtonDown(InputNames.BtnAction6))
        {
            physCtrl.RestoreGravity();
            int dir = (physCtrl.facingDir == PhysicController.facing.Right ? -1 : 1);
            physCtrl.facingDir = (physCtrl.facingDir == PhysicController.facing.Right ? physCtrl.facingDir = PhysicController.facing.Left : physCtrl.facingDir = PhysicController.facing.Right);
            physCtrl.falling = false;
            physCtrl.touchingWall = false;
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_DASH);
            return;
        }
    }

    public override void OnEnterState()
    {
        if (playerRef == null)
            playerRef = GetComponentInParent<Character>().player;

        //cancel previous animation
        //stop movements
        physCtrl.velocity.x = 0;
        physCtrl.velocity.y = 0;
        
        //change gravity settings
        physCtrl.ApplyGravity = WallJumpGravity;
    }

    public override void OnExitState()
    {
        physCtrl.RestoreGravity();
        slideCounter = 0;
        gravCounter = 0;
        exitWall = false;
        wantJump = false;
        playerData.canWallJump = false;
        Invoke("ResetWallJump",0.3f);
    }

    void ResetWallJump()
    {
        playerData.canWallJump = true;
    }

    public override void StateUpdate()
    {
        //increase the counter before wall gravity kick in
        gravCounter += Time.deltaTime;
        GetInputs();
        VerifyExitState();
    }

    void GetInputs()
    {
        if (wantJump)
        {
            physCtrl.GetComponentInChildren<SFXManager>().PlayJumpSound();
            //restore normal gravty per precaution
            physCtrl.RestoreGravity();
            //get the direction of jump
            int dir = (physCtrl.facingDir == PhysicController.facing.Right ? -1 : 1);
            //reverse the facing dir
            physCtrl.facingDir = (physCtrl.facingDir == PhysicController.facing.Right ? physCtrl.facingDir = PhysicController.facing.Left : physCtrl.facingDir = PhysicController.facing.Right);
            //get the jump vel

            if (playerRef.playerInput.GetAxis(InputNames.Axis_Y1) > -0.1f)
                physCtrl.velocity = new Vector2(0, jumpDir.y);
            else
                physCtrl.velocity = new Vector2(jumpDir.x * dir, jumpDir.y);

            physCtrl.falling = false;
            physCtrl.touchingWall = false;
            return;
        }

        //exit the wall
        if (playerRef.playerInput.GetAxis(InputNames.Axis_Y1) < -0.5f)
        {
            exitWall = true;
        }

        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction1))
        {
            wantJump = true;
        }
    }

    void WallJumpGravity()
    {
        if (gravCounter < timeBeforeSlide)
        {
            physCtrl.velocity.y = 0;
            return;
        }

        slideCounter += Time.deltaTime;
        physCtrl.velocity.y = Mathf.Max(physCtrl.velocity.y - physCtrl.gravity * slideCounter, -slideMaxSpeed);

        if (physCtrl.velocity.y < 0)
            physCtrl.falling = true;
    }
}
