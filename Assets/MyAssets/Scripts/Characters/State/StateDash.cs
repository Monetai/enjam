﻿using UnityEngine;
using System.Collections;

public class StateDash : State
{
    public float strength;
    public float maxTime;
    public float delayBetweenDashs = 0.5f;

    bool exit;

    public int nbChargeMax = 3;
    [HideInInspector]
    public int nbChargeLeft;

    int dashDirX;
    int dashDirY;
    Vector2 velocity;

    BasePlayer playerRef;
    GlobalData playerData;

    Animator ator;
    protected override void Start()
    {
        type = StatesManager.statesType.STATE_DASH;
        base.Start();
        playerRef = GetComponentInParent<Character>().player;
        playerData = GetComponentInParent<GlobalData>();
        ator = physCtrl.GetComponentInChildren<Animator>();


        nbChargeLeft = nbChargeMax;
    }

    public override void OnEnterState()
    {
        if (nbChargeLeft == 0)
        {
            exit = true;
            return;
        }
        if (playerRef == null)
            playerRef = GetComponentInParent<Character>().player;
        
        if (playerRef.playerInput.GetAxis(InputNames.Axis_X1) > 0.3f)
        {
            dashDirX = 1;
        }
        else if(playerRef.playerInput.GetAxis(InputNames.Axis_X1) < -0.3f)
        {
            dashDirX = -1;
        }
        else
        {
            // dashDirX = physCtrl.facingDir == PhysicController.facing.Left ? -1 : 1;
            dashDirX = 0;
        }

        if (playerRef.playerInput.GetAxis(InputNames.Axis_Y1) > 0.3f)
        {
            dashDirY = 1;
        }
        else if (playerRef.playerInput.GetAxis(InputNames.Axis_Y1) < -0.3f)
        {
            dashDirY = -1;
        }
        else
        {
            // dashDirX = physCtrl.facingDir == PhysicController.facing.Left ? -1 : 1;
            dashDirY = 0;
        }

        if (dashDirY == 0 && dashDirX == 0)
        {
            exit = true;
            return;
        }


        ator.SetTrigger("Dash");
        physCtrl.GetComponentInChildren<SFXManager>().PlayDashSound();
        physCtrl.gameObject.BroadcastMessage("ConsumeDash");
        physCtrl.gameObject.BroadcastMessage("AttackDash");

        nbChargeLeft--;

        physCtrl.velocity.x = physCtrl.velocity.y = 0;
        Invoke("StopDash", maxTime);
        physCtrl.OnWallEnter += StopDashOnWallEnter;
        physCtrl.ApplyGravity = null;

        velocity = GetDashVelocity(dashDirX, dashDirY);
    }

    Vector2 GetDashVelocity(int dashX, int dashY)
    {

        Vector2 vel = new Vector2();

        vel.x = strength * dashX;
        vel.y = strength * dashY;

        if (dashDirY != 0 && dashDirX != 0)
        {
            vel = vel / 1.8f;
        }

        return vel;
    }

    public override void OnExitState()
    {
        CancelInvoke("StopDash");
        physCtrl.OnWallEnter -= StopDashOnWallEnter;
        physCtrl.RestoreGravity();
        exit = false;
       
        if (IsInvoking("ReactivateDash") == false)
            Invoke("ReactivateDash", delayBetweenDashs);

        if (physCtrl.velocity.y > 0)
            physCtrl.velocity.y = physCtrl.velocity.y/3;

        physCtrl.gameObject.BroadcastMessage("EndAttack");
    }

    protected override void VerifyExitState()
    {
        if (exit  && physCtrl.touchingWall)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_ONWALLS);

        if (exit && physCtrl.grounded)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_GROUNDED);

        if (exit && !physCtrl.grounded)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_INAIR);
    }

    public override void StateUpdate()
    {
        Move();
        AnimationControl();

        VerifyExitState();
    }

    void AddDash()
    {
        if (nbChargeLeft < 3)
        {
            nbChargeLeft++;
        }
    }

    void Move()
    {
        if (exit)
            return;

        if(physCtrl.DescendingSlope && physCtrl.grounded)
            physCtrl.grounded = false;
        else if( physCtrl.grounded && !physCtrl.DescendingSlope)
            physCtrl.grounded = true;

        physCtrl.velocity = velocity;
    }

    void AnimationControl()
    {
    }

    void ReactivateDash()
    {
    }

    void StopDashOnWallEnter()
    {
        CancelInvoke("StopDash");
        exit = true;
        physCtrl.velocity.x = 0;
    }

    void StopDash()
    {
        CancelInvoke("StopDash");
        exit = true;
    }
}
