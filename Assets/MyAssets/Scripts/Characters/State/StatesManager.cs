﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatesManager : MonoBehaviour
{
    Dictionary<statesType, State> states;

    [HideInInspector]
    public State prevState;
    [HideInInspector]
    public State currentState;

    private bool haveChangeState;

    [HideInInspector]
    public PhysicController physCtrl;

    [HideInInspector]
    public enum statesType
    {
        STATE_GROUNDED,
        STATE_INAIR,
        STATE_ONWALLS,
        STATE_DASH,
        STATE_HURT
    }

    public statesType initialState;

    void Awake()
    {
        states = new Dictionary<statesType, State>();
        physCtrl = GetComponentInParent<PhysicController>();
    }

	// Use this for initialization
	void Start ()
    {
        //on laisse le temps aux States de s'enregistrer
        Invoke("Init", 0.1f);
    }

    void StateUpdate()
    {
        haveChangeState = false;
        currentState.StateUpdate();
    }

    private void Init()
    {
        if (!states.TryGetValue(initialState, out currentState))
        {
            Debug.LogError("Cannot Find Initial State");
        }
        currentState.enabled = true;
        currentState.OnEnterState();

        physCtrl.UpdateBehaviour += StateUpdate;
    }

    public void AddState(statesType type, State state)
    {
        if(states.ContainsKey(type))
        {
            Debug.LogError("State " + type + " already added to stateManager");
            return;
        }

        states.Add(type, state);
    }

    public void ChangeCurrentState(statesType newState)
    {
        if (haveChangeState)
            return;

        haveChangeState = true;

        //Sortie du state
        currentState.OnExitState();
        currentState.enabled = false;

        prevState = currentState;

        if (!states.TryGetValue(newState, out currentState))
        {
            Debug.LogError("Cannot find state " + newState);
            return;
        }

        //entree du nouveau state
        currentState.enabled = true;
        currentState.OnEnterState();

        currentState.StateUpdate();
    }

}
