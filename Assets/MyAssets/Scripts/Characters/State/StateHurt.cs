﻿using UnityEngine;
using System.Collections;

public class StateHurt: State
{
    bool exit = false;

    public GameObject toInstantiate;
    GameObject instance;
    public Transform posToInstantiate;

    protected override void Start()
    {
        type = StatesManager.statesType.STATE_HURT;
        base.Start();
    }

    public override void OnEnterState()
    {
        physCtrl.OnCeilingEnter += Stop;
        physCtrl.OnWallEnter += Stop;
        physCtrl.OnGroundEnter += Stop;

        instance = Instantiate(toInstantiate) as GameObject;
        instance.transform.parent = transform;
        instance.transform.position = posToInstantiate.position;

        exit = false;
        Invoke("ExitState",3f);
    }

    public override void OnExitState()
    {
        physCtrl.OnCeilingEnter -= Stop;
        physCtrl.OnWallEnter -= Stop;
        physCtrl.OnGroundEnter -= Stop;
        Destroy(instance.gameObject);
        exit = false;
    }

    void ExitState()
    {
        exit = true;
    }

    protected override void VerifyExitState()
    {
        if (exit && physCtrl.grounded)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_GROUNDED);

        if (exit && !physCtrl.grounded)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_INAIR);
    }

    public override void StateUpdate()
    {
        VerifyExitState();
    }

    void Stop()
    {
        physCtrl.velocity = new Vector2(0, 0);
    }
}
