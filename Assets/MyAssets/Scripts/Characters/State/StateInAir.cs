﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;

public class StateInAir : State
{
    public float acceleration = 4f;
    public float maxSpeed = 150f;
    BasePlayer playerRef;
    GlobalData playerData;
    Animator ator;

    protected override void Start()
    {
        type = StatesManager.statesType.STATE_INAIR;
        base.Start();
        playerRef = GetComponentInParent<Character>().player;
        playerData = GetComponentInParent<GlobalData>();
        ator = physCtrl.GetComponentInChildren<Animator>();

    }

    void OnCeillingCollision()
    {
        physCtrl.velocity.y = 0;
    }

    public override void OnEnterState()
    {
        ator.SetBool("Grounded",false);
        physCtrl.OnCeilingEnter += OnCeillingCollision;
    }

    public override void OnExitState()
    {
        physCtrl.OnCeilingEnter -= OnCeillingCollision;

    }

    protected override void VerifyExitState()
    {
        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction5) || playerRef.playerInput.GetButtonDown(InputNames.BtnAction6))
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_DASH);
        }

        if (physCtrl.grounded)
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_GROUNDED);
        }

        if (physCtrl.grounded == false && physCtrl.touchingWall == true && playerData.canWallJump && physCtrl.velocity.y < 0 && physCtrl.velocity.x != 0)
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_ONWALLS);
        }
    }

    void LateralMovementInputs()
    {
        int nbMaintainedDir = 0;
        int horizontalAxis = 0;
        float xAxis = 0;

        if (playerRef == null)
            playerRef = GetComponentInParent<Character>().player;
        else
            xAxis = playerRef.playerInput.GetAxis(InputNames.Axis_X1);

        //get lateral movement notify if direction changed
        if (xAxis < -0.1f)
        {
            horizontalAxis = -1;
            nbMaintainedDir++;
        }

        if (xAxis > 0.1f)
        {
            horizontalAxis = 1;
            nbMaintainedDir++;
        }

        if (nbMaintainedDir == 2)
        {
            horizontalAxis = 0;
        }

        if (horizontalAxis != 0)
        {
            physCtrl.velocity.x += (acceleration * horizontalAxis) * Time.deltaTime;
            physCtrl.velocity.x = Mathf.Clamp(physCtrl.velocity.x, -maxSpeed, maxSpeed);
        }

    }

    public override void StateUpdate()
    {
        LateralMovementInputs();

        if (playerRef.playerInput.GetButtonUp(InputNames.BtnAction1))
            if (physCtrl.velocity.y > 0)
                physCtrl.velocity.y = 0;

        //if attack
        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction3))
        {
            physCtrl.gameObject.BroadcastMessage("AttackCut");
        }

        VerifyExitState();
    }
}
