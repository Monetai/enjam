﻿using UnityEngine;
using System;
using System.Collections;

public abstract class State : MonoBehaviour {

    [HideInInspector]
    public StatesManager stateManager;
    [HideInInspector]
    public PhysicController physCtrl;
    [HideInInspector]
    public StatesManager.statesType type;

    protected virtual void Start()
    {
        stateManager = GetComponentInParent<StatesManager>();
        physCtrl = stateManager.physCtrl;
        stateManager.AddState(type, this);
        enabled = false;
    }

    protected abstract void VerifyExitState();

    //Optionals methods

    //if the state must interact with physics
    public virtual void StateUpdate()
    {

    }

    //It's a Start Like method
    public virtual void OnEnterState()
    {

    }

    //It's an OnDestroy Like method
    public virtual void OnExitState()
    {

    }

}
