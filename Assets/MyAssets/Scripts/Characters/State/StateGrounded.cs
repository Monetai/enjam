﻿using UnityEngine;
using System.Collections.Generic;
using com.ootii.Messages;

public class StateGrounded : State
{
    public float acceleration = 4f;
    public float maxSpeed = 150f;

    float jumpStrenght;

    public float jumpHeight = 440;
    public float timeToJumpApex = .45f;

    BasePlayer playerRef;
    Animator ator;

    protected override void Start()
    {
        type = StatesManager.statesType.STATE_GROUNDED;
        base.Start();
        physCtrl.gravity = (2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpStrenght = Mathf.Abs(stateManager.physCtrl.gravity) * timeToJumpApex;
        ator = physCtrl.GetComponentInChildren<Animator>();
    }

    public override void OnEnterState()
    {
        ator.SetBool("Grounded", true);
    }

    public override void OnExitState()
    {

    }

    protected override void VerifyExitState()
    {
        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction5) || playerRef.playerInput.GetButtonDown(InputNames.BtnAction6))
        {
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_DASH);
        }

        if (physCtrl.grounded == false)
            stateManager.ChangeCurrentState(StatesManager.statesType.STATE_INAIR);
    }

    void LateralMovementInputs()
    {
        int nbMaintainedDir = 0;
        int horizontalAxis = 0;
        float xAxis = 0;

        if (playerRef == null)
            playerRef = GetComponentInParent<Character>().player;
        else
            xAxis = playerRef.playerInput.GetAxis(InputNames.Axis_X1);

        //get lateral movement notify if direction changed
        if (xAxis < -0.1f)
        {
            horizontalAxis = -1;
            nbMaintainedDir++;
        }

        if (xAxis > 0.1f)
        {
            horizontalAxis = 1;
            nbMaintainedDir++;
        }

        if (nbMaintainedDir == 2)
        {
            horizontalAxis = 0;
        }

        if (horizontalAxis != 0)
        {
            physCtrl.velocity.x += (acceleration * horizontalAxis) * Time.deltaTime;
            physCtrl.velocity.x = Mathf.Clamp(physCtrl.velocity.x, -maxSpeed, maxSpeed);
        }
        else
        {
            physCtrl.velocity.x = 0;
        }

    }

    public override void StateUpdate()
    {
        LateralMovementInputs();

        //if saut
        if(playerRef.playerInput.GetButtonDown(InputNames.BtnAction1))
        {
            physCtrl.GetComponentInChildren<SFXManager>().PlayJumpSound();
            physCtrl.velocity.y = jumpStrenght;
            physCtrl.grounded = false;
        }

        //if attack
        if (playerRef.playerInput.GetButtonDown(InputNames.BtnAction3))
        {
            
            physCtrl.gameObject.BroadcastMessage("AttackCut");
        }

        VerifyExitState();
    }
}

