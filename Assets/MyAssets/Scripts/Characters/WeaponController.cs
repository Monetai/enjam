﻿using UnityEngine;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

public class WeaponController : MonoBehaviour
{
    PhysicController physCtrl;
    Animator ator;
    public Vector2 EjectForce;
    public Collider2D left;
    public Collider2D right;
    AttackType attackType;

    public enum AttackType
    {
        Dash,
        Cut
    }

	// Use this for initialization
	void Start ()
    {
        EndAttack();
        physCtrl = GetComponentInParent<PhysicController>();
        ator = physCtrl.GetComponentInChildren<Animator>();

    }

    void AttackCut()
    {
        attackType = AttackType.Cut;
        physCtrl.GetComponentInChildren<SFXManager>().PlayCutSound();
        ator.SetTrigger("Cut");

        if (physCtrl.facingDir == PhysicController.facing.Left)
            left.enabled = true;
        else
            right.enabled = true;
    }

    void AttackDash()
    {
        attackType = AttackType.Dash;

        if (physCtrl.facingDir == PhysicController.facing.Left)
            left.enabled = true;
        else
            right.enabled = true;
    }

    void EndAttack()
    {
        left.enabled = false;
        right.enabled = false;
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        PhysicController colCtrl = col.GetComponentInParent<PhysicController>();

        if (col.gameObject.tag == "Flower")
        {
            if (physCtrl.GetComponent<Character>().player.GrabbedFlowers.Count >= 5)
                return;

            Flower flower = col.gameObject.GetComponent<Flower>();
            flower.Cut();

            if(flower.flowerPreset.type == FlowerPresets.Type.Dash)
            {
                physCtrl.BroadcastMessage("AddDash", flower.flowerPreset.icon);
                Destroy(flower.gameObject);

                physCtrl.GetComponentInChildren<SFXManager>().PlayGetFlowerSound();
            }
            else
            {
                physCtrl.GetComponent<Character>().player.GrabbedFlowers.Add(flower.flowerPreset);
                physCtrl.BroadcastMessage("AddFlower", flower.flowerPreset.icon);
                Destroy(flower.gameObject);

                physCtrl.GetComponentInChildren<SFXManager>().PlayGetFlowerSound();
            }
        }

        if (attackType == AttackType.Cut)
            return;

        if (col.gameObject.tag == "Weapon")
        {
            ProCamera2DShake.Instance.Shake();

            physCtrl.gameObject.GetComponentInChildren<StatesManager>().ChangeCurrentState(StatesManager.statesType.STATE_HURT);
            colCtrl.gameObject.GetComponentInChildren<StatesManager>().ChangeCurrentState(StatesManager.statesType.STATE_HURT);

            physCtrl.velocity = new Vector2(EjectForce.x * GetInvertFacing(physCtrl), EjectForce.y);
            colCtrl.velocity = new Vector2(EjectForce.x * GetInvertFacing(colCtrl), EjectForce.y);

            physCtrl.grounded = false;
            colCtrl.grounded = false;

            physCtrl.BroadcastMessage("RemoveAllFlower");
            colCtrl.BroadcastMessage("RemoveAllFlower");

            physCtrl.SendMessage("DropFlowers");
            colCtrl.SendMessage("DropFlowers");

            colCtrl.GetComponentInChildren<Animator>().SetTrigger("Hit");
            ator.SetTrigger("Hit");


            physCtrl.GetComponentInChildren<SFXManager>().PlayHurtSounds();
            colCtrl.GetComponentInChildren<SFXManager>().PlayHurtSounds();

        }


        if (col.gameObject.tag == "Player")
        {
            colCtrl.gameObject.GetComponentInChildren<StatesManager>().ChangeCurrentState(StatesManager.statesType.STATE_HURT);
            colCtrl.velocity = new Vector2(EjectForce.x * GetFacing(physCtrl), EjectForce.y);
            colCtrl.grounded = false;

            colCtrl.BroadcastMessage("RemoveAllFlower");
            colCtrl.SendMessage("DropFlowers");

            colCtrl.GetComponentInChildren<Animator>().SetTrigger("Hit");

            colCtrl.GetComponentInChildren<SFXManager>().PlayHurtSounds();

        }
    }

    int GetInvertFacing(PhysicController ctrl)
    {
        if (ctrl.facingDir == PhysicController.facing.Left)
            return 1;
        else
            return -1;
    }

    int GetFacing(PhysicController ctrl)
    {
        if (ctrl.facingDir == PhysicController.facing.Left)
            return -1;
        else
            return 1;
    }
}
