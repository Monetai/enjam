﻿using UnityEngine;
using System.Collections;

public class RaycastUtils
{

    public static readonly int collideAll;
    public static readonly int upRay;
    public static readonly int downRay;
    public static readonly int rightRay;
    public static readonly int leftRay;
    public static readonly int horizontalRay;
    public static readonly int onlyAnywhere;

    static RaycastUtils()
    {
        collideAll = 1 << LayerMask.NameToLayer("CollideAnywhere")
            | 1 << LayerMask.NameToLayer("CollideFromBottom")
            | 1 << LayerMask.NameToLayer("CollideFromTop")
            | 1 << LayerMask.NameToLayer("CollideFromRight")
            | 1 << LayerMask.NameToLayer("CollideFromLeft");

        upRay = 1 << LayerMask.NameToLayer("CollideAnywhere")
            | 1 << LayerMask.NameToLayer("CollideFromBottom");

        downRay = 1 << LayerMask.NameToLayer("CollideAnywhere")
            | 1 << LayerMask.NameToLayer("CollideFromTop")
            | 1 << LayerMask.NameToLayer("CollideFromRight")
            | 1 << LayerMask.NameToLayer("CollideFromLeft");

        leftRay = 1 << LayerMask.NameToLayer("CollideAnywhere")
            | 1 << LayerMask.NameToLayer("CollideFromRight");

        rightRay = 1 << LayerMask.NameToLayer("CollideAnywhere")
            | 1 << LayerMask.NameToLayer("CollideFromLeft");

        horizontalRay = 1 << LayerMask.NameToLayer("CollideAnywhere");

        onlyAnywhere = 1 << LayerMask.NameToLayer("CollideAnywhere");
    }

    public static bool Raycast2D(Vector3 origin, Vector3 direction, float maxDistance, out RaycastHit2D hitInfo2D, int layerMask, Color debugCol)
    {
        hitInfo2D = Physics2D.Raycast(origin, direction, maxDistance, layerMask);
        Debug.DrawRay(origin, direction * maxDistance, debugCol);
        return hitInfo2D;
    }

    public static bool Raycast2D(Vector3 origin, Vector3 direction, float maxDistance, out RaycastHit2D hitInfo2D, int layerMask)
    {
        hitInfo2D = Physics2D.Raycast(origin, direction, maxDistance, layerMask);
        return hitInfo2D;
    }
}

