﻿using UnityEngine;
using System.Collections;

public class AnimController : MonoBehaviour {

    PhysicController physCtrl;
    Animator ator;

	// Use this for initialization
	void Start ()
    {
        physCtrl = GetComponentInParent<PhysicController>();
        ator = GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update ()
    {
        if (physCtrl.facingDir == PhysicController.facing.Left)
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
        else
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);

        if (physCtrl.velocity.x != 0 && physCtrl.grounded)
            ator.SetBool("Moving", true);
        else
            ator.SetBool("Moving", false);


    }

    public void EndAttackEvent()
    {
        physCtrl.gameObject.BroadcastMessage("EndAttack");
    }

    public void FootStep()
    {
        physCtrl.GetComponentInChildren<SFXManager>().PlayFootStep();
    }
}
