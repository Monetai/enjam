﻿using UnityEngine;
using System.Collections;


public static class InputNames {
	
	public static readonly string Axis_X1 = "AnalogX1";  //leftstick X
    public static readonly string Axis_Y1 = "AnalogY1";    //leftstick Y

    public static readonly string Axis_X2 = "AnalogX2";  //rightstick X
    public static readonly string Axis_Y2 = "AnalogY2";    //rightstick Y

    public static readonly string Axis_Z1 = "AnalogZ";       //triggers 
    public static readonly string Axis_ZPos = "AnalogZ+";
    public static readonly string Axis_ZNeg = "AnalogZ-";

    public static readonly string BtnAction1 = "Action1";   //A
    public static readonly string BtnAction2 = "Action2";   //B
    public static readonly string BtnAction3 = "Action3";   //X
    public static readonly string BtnAction4 = "Action4";   //Y

    public static readonly string BtnAction5 = "Action5";   //L
    public static readonly string BtnAction6 = "Action6";   //R

    public static readonly string BtnAction7 = "Action7";   //LStick
    public static readonly string BtnAction8 = "Action8";   //RStick

    public static readonly string BtnUp = "D-Up";   
    public static readonly string BtnDown = "D-Down"; 
    public static readonly string BtnLeft = "D-Left";  
    public static readonly string BtnRight = "D-right";

    public static readonly string BtnStart = "Start";   //LStick
    public static readonly string BtnBack = "Back";   //RStick

}
