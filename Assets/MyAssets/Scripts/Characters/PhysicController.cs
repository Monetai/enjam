﻿using UnityEngine;
using System;
using System.Collections;


//base class for the arcade-like physics of Ilios
//this class handle:
//-the application of gravity
//-collision with ground, walls and celling

//the following is in french beacause I suck in English
/*
*Cette classe a une responsabilité limitée mais reste assez complete, elle gere les raycast pour
*detecter les collisions sur l'envionnement (en gerant plusieurs layers, pour pouvoir par exemple faire des one-way platforms)
*et appelle un delegate pour gerer le comportement en cas de colision, il est donc facile de changer un comportement en ajoutant, supprimant
*des fonctions assossies aux delegates, il est donc possible aussi de lancer des effets et animations a l'appel de ces delegates, et bien sur 
*un comportement peut être ajoute en tant que monobehaviour attache a un gameObject.
*
*Exemple:

    WallJumpBehaviour

    //j'ajoute l'appel a BeginWallJump lorsque le personnage rentre en collision avec un mur
    PhysicsController.OnEnterCollisionWithWall += WallJumpBehaviour.BeginWallJump;

    BeginWallJump()
    {
        //si l'on est en l'air et que l'on peut wallump alors on switche la gravite pour mettre celle adapte au walljump
        if(!grounded && canWallJump)
        physicController.ApplyGravity = WallJumpBehaviour.StickGravity; //on enleve la gravite de base, il faudra la remettre a la sortie du stick

        //pas de gestion de la correction de la position car ce comportement est le comportement de base appelé avec le delegate
        //il est possible de le retirer afin de mettre un comportement spécial
    }

*Le but de ce systeme est de pouvoir se passer de variables d'etats complexes à maintenirs et qui sont assez difficile a scaler et a generaliser
*l'intention et de pouvoir rendre aussi simple que possible l'extention de cette classe de base tout en permettant une liberté d'actions assez grande
*et une maintenabilité interessante en appliquant au maximum le principe de responsabilité limité.
*/

/*
TODO:
-add properties for states variables and velocity
*/
public class PhysicController : MonoBehaviour
{
    //facing direction
    [HideInInspector]
    public enum facing { Right, Left }
    [HideInInspector]
    public facing facingDir;

    [Header("Gravity Settings (units/second)")]
    public float gravity = 9.81f;
    public float maxFallSpeed = 200f;

    [Header("Lateral movement Settings (units/second)")]
    public float groundDamping = 4f;
    public float airDamping = 4f;
    public float maxClimbAngle = 45f;
    public float maxDescendAngle = 70f;

    [Header("Collision Detection Settings")]
    public int nbRaysHorizontal = 5;
    public int nbRaysVertical = 5;
    public float rayOffset = 2;
    public float horizontalRayLenghtMultiplier;
    public float verticalRayLenghtMultiplier;
    public float skinWidth = .1f;

    protected float RaycastEspilon = 0.01f;
    protected Vector3 center;
    protected float minDistance = Mathf.Infinity;
    protected int indexTouch = 0;

    [HideInInspector]
    public RaycastHit2D hitInfo2D; //Be carefull it's a sort of global variable don't mess with it
    [HideInInspector]
    public RaycastHit2D[] VerticalhitInfos2D; //Be carefull it's a sort of global variable don't mess with it
    [HideInInspector]
    public RaycastHit2D[] HorizontalhitInfos2D; //Be carefull it's a sort of global variable don't mess with it

    //mask for collisions
    [HideInInspector]
    public int topMask;
    [HideInInspector]
    public int bottomMask;
    [HideInInspector]
    public int leftMask;
    [HideInInspector]
    public int rightMask;

    // the mask for raycast
    //protected int layerMask;

    //state variables
    [HideInInspector]
    public bool grounded = false;
    [HideInInspector]
    public bool falling = false;
    [HideInInspector]
    public bool touchingWall = false;
    [HideInInspector]
    public bool touchingCeilling = false;
    [HideInInspector]
    public bool ClimbingSlope = false;
    [HideInInspector]
    public bool DescendingSlope = false;

    //velocity of the object
    [HideInInspector]
    public Vector2 velocity = Vector2.zero;
    [HideInInspector]
    public Vector2 tempVelocity = Vector2.zero;

    //cache the transform
    protected Transform myTransform;
    //cache the collider
    protected Collider2D col;
    //store collider boundaries
    [HideInInspector]
    public Rect bound;

    //temp var to avoid multiples new in the update loop
    Vector2 tempVect2 = Vector2.zero;
    Vector3 tempVect3 = Vector2.zero;
    Vector3 originSerie = Vector3.zero;
    Vector3 endSerie = Vector3.zero;
    Vector3 originCast = Vector3.zero;

    float detectionEpsilon = 50f;
    public float slopeAngle;

    // delegates

    // Ground callback
    public Action OnGroundEnter;
    public Action OnGroundStay;
    public Action OnGroundExit;
    // Walls callback
    public Action OnWallEnter;
    public Action OnWallStay;
    public Action OnWallExit;
    // Ceiling callback
    public Action OnCeilingEnter;
    public Action OnCeilingStay;
    public Action OnCeilingExit;

    public Action ApplyGravity;
    public Action UpdateBehaviour;

    void Awake()
    {
        VerticalhitInfos2D = new RaycastHit2D[nbRaysVertical];
        HorizontalhitInfos2D = new RaycastHit2D[nbRaysHorizontal];

        topMask = RaycastUtils.upRay;
        bottomMask = RaycastUtils.downRay;
        leftMask = RaycastUtils.horizontalRay;
        rightMask = RaycastUtils.horizontalRay;
    }

    void Start()
    {
        //cache
        myTransform = transform;

        //attache the base gravity
        ApplyGravity += BaseGravity;
    }

    //physics related computation must be done in FixedUpdate
    void Update()
    {
        //get a rect for facilitate further computations
        UpdateColliderInfos();

        if (UpdateBehaviour != null)
            UpdateBehaviour();
        //the gravity function is a delegate to change physics settings on the fly
        if (ApplyGravity != null)
            ApplyGravity();


        tempVelocity = velocity * Time.deltaTime;

        //colision handling and callbacks calling
        HaveTouchLateral();

        if (tempVelocity.y > 0)
        HaveTouchCeilling();
        else
        IsGrounded();
    }

    //apply the movemement after everything to be more predictable
    void LateUpdate()
    {
        transform.Translate(tempVelocity);
    }

    public void RestoreGravity()
    {
        ApplyGravity = BaseGravity;
    }

    protected void UpdateColliderInfos()
    {
        if (col == null)
            col = GetComponent<Collider2D>();

        bound = new Rect(
            col.bounds.min.x,
            col.bounds.min.y,
            col.bounds.size.x,
            col.bounds.size.y
            );

        if (velocity.x > 0)
            facingDir = facing.Right;
        else if (velocity.x < 0)
            facingDir = facing.Left;
    }

    //**** Collisions Handlers ****

    protected void IsGrounded()
    {
        if (grounded || falling )
            if (CheckVerticalDirection(Vector3.down))
            {
                VerticalDownPositionCorrection();

                if (grounded == false)
                {
                    if (OnGroundEnter != null)
                        OnGroundEnter();

                    grounded = true;
                    falling = false;
                    touchingCeilling = false;
                }
                else
                {
                    if (OnGroundStay != null)
                        OnGroundStay();
                }
            }
            else
            {
                if (grounded == true)
                {
                    if (OnGroundExit != null)
                        OnGroundExit();
                    grounded = false;
                }
            }

    }

    protected void HaveTouchLateral()
    {
        if (tempVelocity.x != 0 || touchingWall || !grounded)
            if (CheckLateral())
            {
                //if(!touchingWall && !grounded)
                HorizontalPositionCorrection();

                float slopeAngle = Vector2.Angle(HorizontalhitInfos2D[indexTouch].normal, Vector2.up);
                if (Mathf.Abs(Mathf.Abs(slopeAngle) - 90) <= 3)
                {
                    if (touchingWall == false)
                        if (OnWallEnter != null)
                            OnWallEnter();

                    touchingWall = true;
                }
            }
            else
            {
                ClimbingSlope = false;
                if (touchingWall == true)
                    if (OnWallExit != null)
                        OnWallExit();

                touchingWall = false;
            }
        else if( touchingWall )
        {
            if (OnWallStay != null)
                OnWallStay();
        }

    }

    protected void HaveTouchCeilling()
    {
        if (CheckVerticalDirection(Vector3.up))
        {
            VerticalUpPositionCorrection();

            if (touchingCeilling == false)
            {
                if (OnCeilingEnter != null)
                    OnCeilingEnter();

                touchingCeilling = true;

            }
            else
            {
                if (OnCeilingStay != null)
                    OnCeilingStay();

                touchingCeilling = true;
            }

        }
        else
        {
            if (touchingCeilling == true)
            {
                if (OnCeilingExit != null)
                    OnCeilingExit();

                touchingCeilling = false;
            }
            touchingCeilling = false;
        }
    }

    //**** Collisions Checks ****
    //maybe refactor in 1 function checkVertical and CheckHorizontal for better maintability

    protected bool CheckVerticalDirection(Vector3 direction)
    {
        //cast several ray downward between borders of the collider

        //origin of raycast serie
        originSerie.x = bound.xMin + rayOffset;
        originSerie.y = direction.y <= 0 ?  bound.yMin + skinWidth : bound.yMax - skinWidth;
        originSerie.z = myTransform.position.z;
        //end of raycast serie
        endSerie.x = bound.xMax - rayOffset;
        endSerie.y = direction.y <= 0 ? bound.yMin + skinWidth : bound.yMax - skinWidth;
        endSerie.z = myTransform.position.z;

        float distance = skinWidth * 2 + (grounded ?  rayOffset * verticalRayLenghtMultiplier : Mathf.Abs(tempVelocity.y) * verticalRayLenghtMultiplier);

        minDistance = Mathf.Infinity;
        indexTouch = 0;
        int layer = direction == Vector3.up ? topMask : bottomMask;

        bool haveTouched = false;
        for (int i = 0; i < nbRaysVertical; i++)
        {
            float amount = (float)i / (nbRaysVertical - 1);
            originCast = Vector3.Lerp(originSerie, endSerie, amount);

            if (RaycastUtils.Raycast2D(originCast, direction, distance, out VerticalhitInfos2D[i], layer, Color.blue))
            {
                haveTouched = true;
                if (VerticalhitInfos2D[i].distance < minDistance)
                {
                    minDistance = VerticalhitInfos2D[i].distance;
                    indexTouch = i;
                }
            }
        }
        return haveTouched;

    }

    protected bool CheckHorizontalDirection(Vector3 direction)
    {
        //cast several ray downward between borders of the collider

        //origin of raycast serie
        originSerie.x = direction.x <= 0 ? bound.xMin + skinWidth : bound.xMax - skinWidth;
        originSerie.y = bound.yMin + rayOffset;
        originSerie.z = myTransform.position.z;
        //end of raycast serie
        endSerie.x = direction.x <= 0 ? bound.xMin + skinWidth : bound.xMax - skinWidth;
        endSerie.y = bound.yMax - rayOffset;
        endSerie.z = myTransform.position.z;

        float distance = skinWidth * 2 + Mathf.Abs(tempVelocity.x) * horizontalRayLenghtMultiplier;

        minDistance = Mathf.Infinity;
        indexTouch = 0;
        int layer = direction == Vector3.right ? rightMask : leftMask;

        bool haveTouched = false;
        for (int i = 0; i < nbRaysVertical; i++)
        {
            float amount = (float)i / (nbRaysVertical - 1);
            originCast = Vector3.Lerp(originSerie, endSerie, amount);

            if (RaycastUtils.Raycast2D(originCast, direction, distance, out HorizontalhitInfos2D[i], layer, Color.red))
            {
                haveTouched = true;
                if (HorizontalhitInfos2D[i].distance < minDistance)
                {
                    minDistance = HorizontalhitInfos2D[i].distance;
                    indexTouch = i;
                }
            }
        }
        return haveTouched;
    }

    //check collision in the direction of velocity
    protected bool CheckLateral()
    {
        //compute the direction
        if (touchingWall ||  tempVelocity.x == 0)
            tempVect3 = facingDir == facing.Right ? Vector3.right : Vector3.left;
        else
        tempVect3 = tempVelocity.x >= 0 ? Vector3.right : Vector3.left;

        return CheckHorizontalDirection(tempVect3);
    }

    //**** Delegates functions ****
    //Base Gravity 
    protected void BaseGravity()
    {
        //if character not on ground apply gravity
        if (grounded == false)
        {
            tempVect2.x = velocity.x;
            tempVect2.y = Mathf.Max(velocity.y - (gravity * Time.deltaTime), -maxFallSpeed);
            velocity = tempVect2;
        }
        else
        {
            velocity.y = 0;
        }

        float limit = grounded ? groundDamping * Time.deltaTime : airDamping * Time.deltaTime;
        if (Mathf.Abs(velocity.x) >= limit)
        {
            int direction = velocity.x > 0 ? -1 : 1;

            if (grounded)
                velocity.x += (groundDamping * direction) * Time.deltaTime;
            else
                velocity.x += (airDamping * direction) * Time.deltaTime;
        }
        else
        {
            velocity.x = 0;
        }

        if (velocity.y < 0 && !grounded)
        {
            falling = true;
        }
    }

    //gerer ici la descente de slope pour eviter les petits bonds

    virtual protected void VerticalDownPositionCorrection()
    {
        float directionX = (facingDir == facing.Right ? 1 : -1);

        float slopeAngle = Vector2.Angle(VerticalhitInfos2D[indexTouch].normal, Vector2.up);

        if (Mathf.Abs(slopeAngle) >= 5 && slopeAngle <= maxDescendAngle)
        {
            if (Mathf.Sign(VerticalhitInfos2D[indexTouch].normal.x) == directionX)
            {
                DescendingSlope = true;

                //transform.Translate(tempVelocity);

                float moveDistance = Mathf.Abs(tempVelocity.x);
                float descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                tempVelocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * (facingDir == facing.Right ? 1 : -1);
                tempVelocity.y -= descendVelocityY;

                //tempVelocity.x = 0;
            }
            else
            {
                DescendingSlope = false;
            }
        }
        else
        {
            DescendingSlope = false;
        }

        if(!DescendingSlope)
        tempVelocity.y = (VerticalhitInfos2D[indexTouch].distance - skinWidth) * (tempVelocity.y > 0 ? 1 : -1);
    }

    virtual protected void VerticalUpPositionCorrection()
    {
        tempVelocity.y = 0;
    }

    virtual protected void HorizontalPositionCorrection()
    {
        slopeAngle = Vector2.Angle(HorizontalhitInfos2D[indexTouch].normal, Vector2.up);
        //si le ray le plus bas touche une slope
        if (indexTouch == 0 && slopeAngle < maxClimbAngle)
        {
            if(grounded)
            {
                ClimbingSlope = true;
                ClimbSlope(slopeAngle);
            }
        }
        else
        {
            ClimbingSlope = false;
            tempVelocity.x = (HorizontalhitInfos2D[indexTouch].distance - skinWidth) * (facingDir == facing.Right ? 1 : -1);
        }
    }

    virtual protected void ClimbSlope(float angle)
    {
        float moveDistance = Mathf.Abs(tempVelocity.x);
        tempVelocity.y = Mathf.Sin(angle * Mathf.Deg2Rad) * moveDistance;
        tempVelocity.x = Mathf.Cos(angle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(tempVelocity.x);

    }

    
    public void SwapFacing()
    {
        facingDir = (facingDir == facing.Right ? facing.Left : facing.Right);
    }
}

