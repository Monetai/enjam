﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    [Range(1, 4)]
    public int slot = 1;
    [HideInInspector]
    public BasePlayer player;

	[SerializeField]
	bool isNearVan;

	[SerializeField]
	private float prevRightTriggerPressure;
	[SerializeField]
	private float rightTriggerPressure;
	[SerializeField]
	private float pressureDiff;

	[SerializeField]
	private float cumulativePressure;
	[SerializeField]
	private float timeTo100;
	[SerializeField]
	private float lastTimeTo100;


	void Start()
    {
        PlayerManager.Instance.Characters[slot - 1] = this;
        player = PlayerManager.Instance.Players[slot - 1];

		//TestFlowers();
    }

	public int testFlowers=5;
	void TestFlowers()
	{
		var flower = Instantiate(Resources.Load<Flower>("Prefabs/Flower"));

		for (int i=0; i< testFlowers;i++)
		if (player.TryAddFlower(flower.flowerPreset)) {
			Destroy(flower.gameObject);
		}

		DropFlowers();
	}

    void LateUpdate()
    {
        Fill();
    }

	public void Fill()
	{
        GetComponentInChildren<Animator>().SetBool("ChargeVan", false);
        if (!isNearVan || player.GrabbedFlowers.Count == 0) return;

		prevRightTriggerPressure = rightTriggerPressure;

		rightTriggerPressure = player.playerInput.GetAxis(InputNames.Axis_Z1);
        if (rightTriggerPressure > 0.00001)
        {
            GetComponentInChildren<Animator>().SetBool("ChargeVan", true);
            pressureDiff = Mathf.Abs(prevRightTriggerPressure - rightTriggerPressure);
            cumulativePressure += pressureDiff * pressureDiff * pressureDiff * 30;
        }
        else
        {
            prevRightTriggerPressure = 0;
            
        }

		if (cumulativePressure > 0) {
			timeTo100 += Time.deltaTime;
		}

		if (cumulativePressure > 100) {
			lastTimeTo100 = timeTo100;
			timeTo100 = 0;
			player.ValidateFillVan();
			cumulativePressure = prevRightTriggerPressure = rightTriggerPressure = 0;
            GetComponentInChildren<SFXManager>().PlayCombiHornSound();
            PlayerManager.Instance.Characters[slot - 1].gameObject.BroadcastMessage("RemoveLast");
		}

	}


	void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Player entered " + other);
        var van = other.GetComponent<HippieVan>();
        if (van && van.assignedPlayer == player)
        {
            isNearVan = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        //Debug.Log("Player is within " + other);
        var van = other.GetComponent<HippieVan>();
        if (van && van.assignedPlayer == player)
        {
			isNearVan = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Debug.Log("Player exited " + other);
        var van = other.GetComponent<HippieVan>();
        if (van && van.assignedPlayer == player)
        {
			isNearVan = false;
        }
    }

	public void CutFlower(Flower flower)
	{
		flower.Cut();
		
	}

	public void PickFlower(Flower flower)
	{
		player.TryAddFlower(flower.flowerPreset);
		flower.Pick();

	}

	public void DropFlowers()
	{
        FlowerPresets f;
		float i, flowerCount = player.GrabbedFlowers.Count;
		
		float gap = flowerCount == 1 ? 1 : 1f / (flowerCount-1);
		i = flowerCount;

		while ( player.TryRemoveFlower(-1,out f)) {

			var d = ((i / flowerCount) - 0.5f)*3;

			FlowerSeed.SpawnSeed(f, transform.position+Vector3.up*0.75f, new Vector2(d,5) );
			//Destroy(f.gameObject);

			i -= 1f;

		}
	}
}
