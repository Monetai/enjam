﻿using UnityEngine;
using System.Collections.Generic;

public class DashUI : MonoBehaviour
{
    public GameObject iconeToInstantiate;
    public List<GameObject> icones;

    void ConsumeDash()
    {
        if (icones.Count != 0)
        {
            GameObject toDestroy = icones[0];
            icones.RemoveAt(0);
            Destroy(toDestroy);
        }
            
    }

    void AddDash()
    {
        if(icones.Count < 3)
        {
            GameObject obj = Instantiate(iconeToInstantiate, gameObject.transform) as GameObject;
            icones.Add(obj);
        }
    }

}
