﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	public 	static PlayerManager Instance { get; private set; }
	[SerializeField]
	private BasePlayer[] _Players;
	public BasePlayer[] Players { get { return _Players; } }

	private Character[] _Characters = new Character[4];
	public Character[] Characters { get { return _Characters; } }

	// Use this for initialization
	void Awake () {
		var players = new BasePlayer[4];

		for (int i = 0; i < _Players.Length; i++) {
			players[i] = _Players[i];
		}
		_Players = players;

		LoadPlayers();

		Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LoadPlayers()
	{
		BasePlayer bpPrefab=Resources.Load<BasePlayer>("Prefabs/Player");
		for (int i=0; i < 4; i++) {
			if (!_Players[i])
				_Players[i] = Instantiate(bpPrefab);

			_Players[i].slot = i + 1;
			_Players[i].transform.SetParent(gameObject.transform);
			_Players[i].name = "Player" + _Players[i].slot;
		}
		
	}
}
