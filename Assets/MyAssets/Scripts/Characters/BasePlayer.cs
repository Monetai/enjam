﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using System;
using Rewired;

public class BasePlayer : MonoBehaviour, IComparable<BasePlayer>{

	static int maxFlowers = 5;
	[SerializeField]
	private List<FlowerPresets> _GrabbedFlowers;
	public List<FlowerPresets> GrabbedFlowers { get { return _GrabbedFlowers; } private set { GrabbedFlowers = value; } }

	[Header("Info")]
	[Range(1,4)]
	public int slot=1;

	public string playerName="_unnamed_";
    public string DisplayName { get { return playerName.Length > 0 ? char.ToUpper(playerName[0]) + playerName.Substring(1) : "Player" + PlayerSuffix;  } }

    public string PlayerPrefix { get { return slot+"P_"; } }
    public string PlayerSuffix { get { return "_P"+slot;}}


	public Rewired.Player playerInput;

	public int filledFlowers;

	virtual protected void Awake()
    {
		DontDestroyOnLoad(transform.gameObject);
		_GrabbedFlowers = new List<FlowerPresets>(maxFlowers);
	}

	virtual protected void Start()
	{
		LoadIngame();
	}

	virtual protected void LateUpdate()
	{

	}

	public bool TryAddFlower(FlowerPresets flower)
	{
		if (GrabbedFlowers.Count < maxFlowers) {
			GrabbedFlowers.Add(flower);
			return true;
		}

		return false;
	}

	public bool TryRemoveFlower(int index, out FlowerPresets removedFlower)
	{
		if (index < 0) index = GrabbedFlowers.Count - 1;
		if (GrabbedFlowers.Count > 0 && index < maxFlowers) {
			removedFlower = GrabbedFlowers[index];
			GrabbedFlowers.RemoveAt(index);
			return true;
		}

		removedFlower = null;
		return false;
	}

	public void ValidateFillVan()
	{
        FlowerPresets f;
		if (TryRemoveFlower(-1,out f)) {
			filledFlowers += f.PointValue;
		}
		
	}

	public void LoadIngame(){

		AssignControllers();
//		AssignHUD();

	}
	
	void AssignControllers(){

		playerInput = ReInput.players.GetPlayer(slot - 1);
		if (playerInput != null && !ReInput.players.GetPlayer(slot - 1).controllers.ContainsController(Rewired.ControllerType.Joystick, slot-1))
		{
			Debug.LogWarning("Player " + slot + " has no Gamepad.");
		}
		else {
			Debug.LogWarning("Player " + slot + " Gamepad connected.");
		}

	}

	virtual protected void AssignHUD(){
		Debug.LogWarning("AssignHUD not overloaded/edited");
	}

	void OnControllerConnected(ControllerStatusChangedEventArgs args){
		if(args.controllerId != slot)
			return;

	}

	void OnControllerDisconnected(ControllerStatusChangedEventArgs args){
		if(args.controllerId != slot)
			return;
	}

	void OnControllerPreDisconnect(ControllerStatusChangedEventArgs args){
		if(args.controllerId != slot)
			return;

	}

	/// <summary>
	/// Sorts players with this priority order: Delivered -> Picked -> Slot number
	/// </summary>
	/// <returns>The to.</returns>
	/// <param name="other">Other.</param>
	public int CompareTo(BasePlayer other)
	{
		var deltaFlowers = other.filledFlowers - filledFlowers;

		return deltaFlowers != 0 ? deltaFlowers: slot - other.slot;
	}

#if UNITY_EDITOR

	void OnValidate()
	{

	}
#endif

}
