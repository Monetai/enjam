﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIFlower : MonoBehaviour {

    public GameObject iconeToInstantiate;
    public List<GameObject> icones;


    void AddFlower( Sprite icons )
    {
        GameObject obj = Instantiate(iconeToInstantiate,gameObject.transform) as GameObject;
        icones.Add(obj);
        obj.GetComponent<Image>().sprite = icons;
    }

    void RemoveLast()
    {
        if (icones.Count != 0)
        {
            GameObject toDestroy = icones[icones.Count-1];
            icones.RemoveAt(icones.Count - 1);
            Destroy(toDestroy);
        }
    }

    void RemoveOneFlower()
    {
        if (icones.Count != 0)
        {
            GameObject toDestroy = icones[0];
            icones.RemoveAt(0);
            Destroy(toDestroy);
        }
    }

    void RemoveAllFlower()
    {
        foreach (GameObject go in icones)
            Destroy(go);

        icones.Clear();
    }
}
