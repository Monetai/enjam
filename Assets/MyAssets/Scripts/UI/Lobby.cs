﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Lobby : MonoBehaviour {

	public Image[] readyButtons;
	protected bool[] readyPlayers=new bool[4];
	protected PlayerManager pManager;

	public SpriteRenderer[] readyFlowers;

	public bool testForReady;

	bool everyoneReady;
	// Use this for initialization
	void Awake()
	{
		pManager = FindObjectOfType<PlayerManager>();
		//readyButtons = GetComponentsInChildren<Image>();
	}
	
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (everyoneReady) return;

        if (Input.GetKeyDown(KeyCode.B))
            everyoneReady = true;


		for (int i=0; i<4; i++) {
			var pressed=pManager.Players[i].playerInput.GetButtonDown(InputNames.BtnAction1);

            if (pressed)
                GetComponent<SoundBank>().PlayRandomClip();

            if (pressed || (i>0 && testForReady && !readyPlayers[i])) {
				readyPlayers[i] = !readyPlayers[i];

				if (readyPlayers[i]) {
					readyButtons[i].color = Color.Lerp(Color.white, Color.grey, 0.5f);
					readyButtons[i].GetComponentInChildren<Text>().text = "Player " + (i + 1) + " ready";
				} else {
					readyButtons[i].color = Color.white;
				}					
			}
		}
	}

	void LateUpdate()
	{
		if (!everyoneReady) {
			int readyCount = 0;

			for (int i = 0; i < 4; i++) {
				var textUI = readyButtons[i].GetComponentInChildren<Text>();
				if (readyPlayers[i]) {
					textUI.text = "Player " + (i + 1) + " is ready";
					readyFlowers[i].enabled=true;
				} else {
					textUI.text = "Waiting for Player " + (i + 1);
					readyFlowers[i].enabled = false;
				}
				if (readyPlayers[i]) readyCount++;
			}

			if (!everyoneReady && readyCount == 4)
				everyoneReady = true;

		} else {
			gameObject.SetActive(false);
			GameRules.Instance.currentState = GameRules.State.BattleOngoing;
            //do loadScence call here
            SceneManager.LoadScene("Arena1");
		}

	}
}
