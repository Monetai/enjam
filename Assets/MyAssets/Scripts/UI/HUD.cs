﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class HUD : MonoBehaviour {

	public Text timerText;
	[Range(0,300)]
	public float redTimerThreshold = 30f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public string TimeToText(float time)
	{
		var ts = TimeSpan.FromSeconds(time);
		return ts.Minutes + ":" + ts.Seconds.ToString("0#");
	}

	void OnGUI()
	{
		var timeleft = GameRules.Instance.RemainingTime;
		timerText.text = TimeToText(timeleft);
		timerText.color = timeleft > redTimerThreshold ? 
			Color.white : Color.Lerp(Color.white, Color.red, Mathf.SmoothStep(1,0,timeleft/ redTimerThreshold));
	}
}
