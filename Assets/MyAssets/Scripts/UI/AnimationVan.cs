﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimationVan : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.DOMoveY (
			transform.position.y + 0.2f,
			0.5f).SetLoops (-1, LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
