﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AnimationTitle : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.DOScale (
			1f,
			0.5f).SetLoops (-1, LoopType.Yoyo);
	}

	// Update is called once per frame
	void Update () {

	}
}
