﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class Scoreboard : MonoBehaviour {

	public Image[] vans = new Image[4];

	[Range(0.1f,10)]
	public float maxAnimDuration=3;

	private float transitionCountdown;

	// Use this for initialization
	void Start () {
		PlayAnim();
	}

	void PlayAnim()
	{
		transitionCountdown = maxAnimDuration + 3f;
		//	var anchor = vans[0].rectTransform.anchoredPosition;
		Canvas cv = GetComponentInParent<Canvas>();
		int maxScore = GameRules.Instance.maxScore;
		for (int i=0; i<4; i++) {
			var filled = PlayerManager.Instance.Players[i].filledFlowers;// = i * 4;
			var ratio = Mathf.Clamp01(PlayerManager.Instance.Players[i].filledFlowers / (float)maxScore);
			vans[i].rectTransform.DOAnchorPosX(cv.pixelRect.xMax * (filled < maxScore ? 2f:4f) * 0.75f* ratio,
				maxAnimDuration* ratio);
		}

	}
	
	// Update is called once per frame
	void Update () {
	
		if (transitionCountdown > 0) {
			transitionCountdown -= Time.deltaTime;

			if (transitionCountdown <= 0) {
				//scoreboard finished
			}
		}
	}
}
