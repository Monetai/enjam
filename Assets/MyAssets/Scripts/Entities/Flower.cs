﻿using UnityEngine;
using System.Collections;

public class Flower : MonoBehaviour
{

	public enum State
	{

		Unspawned,
		Spawning,
		Spawned,
		Cut,
		OnFloor
	}

	public State currentState;

	public SpriteRenderer spriteRenderer;
	public FlowerPresets flowerPreset;
	private FlowerSpawner spawner;

	public Sprite currentSprite;
	Sprite[] currentAnim;
	int frameIndex;

	[Range(0, 60)]
	public float idleFps = 24;
	[Range(0, 60)]
	public float spawningFps = 48;

	float elapsedTime = 0;
	float frameDuration;
	// Use this for initialization

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void Start()
	{

		FpsToDuration(24);
		ChangeState(State.Spawning);

	}

	// Update is called once per frame
	void LateUpdate()
	{
		if (currentAnim == null || currentAnim.Length == 0) return;

		elapsedTime += Time.deltaTime;
		if (elapsedTime >= frameDuration) {
			elapsedTime = 0;
			frameIndex = (frameIndex + 1) % currentAnim.Length;

			if (frameIndex == 0) {
				if (currentState == State.Spawning) {
					ChangeState(State.Spawned);
					GetComponent<SoundBank>().PlayRandomClip();
				}
				else if (currentState == State.Cut) {
					ChangeState(State.OnFloor);
				}
			}
		}

		currentSprite = currentAnim[frameIndex];
		spriteRenderer.sprite = currentSprite;
	}

	public void ChangeType()
	{
		flowerPreset = Resources.Load<FlowerPresets>("Prefabs/TestFlowerPurple");
		ChangeState(currentState);
	}

	public void ChangeState(State nextState)
	{

		elapsedTime = 0;
		frameIndex = 0;

		switch (nextState) {
			case State.Unspawned:
				currentAnim = flowerPreset.spawning; break;
			case State.Spawning:
				FpsToDuration(spawningFps);
				currentAnim = flowerPreset.spawning; break;
			case State.Spawned:
				FpsToDuration(idleFps);
				currentAnim = flowerPreset.spawned; break;
			case State.Cut: currentAnim = flowerPreset.cut; break;
			default: currentAnim = flowerPreset.spawned; break;
		}

		currentState = nextState;
	}

	public static Flower Spawn(Vector2 position, FlowerSpawner spawner = null, FlowerPresets preset = null)
	{
		Flower flower = GameObject.Instantiate<Flower>(Resources.Load<Flower>("Prefabs/Flower"));
		if (preset)
			flower.flowerPreset = preset;
		else
			Resources.Load<Flower>("Prefabs/TestFlowerOrange");

		var ps = Instantiate<ParticleSystem>( flower.flowerPreset.particles);

		if (ps) {
			ps.gameObject.transform.SetParent(flower.transform,false);
			ps.Play();
		}

		flower.spawner = spawner;
		flower.transform.position = position;
		flower.ChangeState(State.Spawning);
		return flower;
	}

	public void Cut()
	{
		if (spawner != null)
			spawner.TryRemove(this);
	}

	public void Pick()
	{
		if (spawner != null)
			spawner.TryRemove(this);
	}

	void FpsToDuration(float fps)
	{
		frameDuration = fps > 0 ? 1f / fps : float.PositiveInfinity;
	}
}
