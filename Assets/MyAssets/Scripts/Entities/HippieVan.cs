﻿using UnityEngine;
using System.Collections;

public class HippieVan : MonoBehaviour {

    [Range(1, 4)]
    public int slot = 1;
    [HideInInspector]
    public BasePlayer assignedPlayer;

    void Start()
    {
        assignedPlayer = PlayerManager.Instance.Players[slot - 1];
    }

}
