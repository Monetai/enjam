﻿using UnityEngine;
using System.Collections;

public class FlowerSeed : MonoBehaviour {

	public FlowerPresets preset;
	public Rigidbody2D rb2D;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void SpawnSeed(FlowerPresets preset, Vector2 position, Vector2 velocity)
	{
		var seed = Instantiate<FlowerSeed>(Resources.Load<FlowerSeed>("Prefabs/FlowerSeed"));
		seed.preset = preset;
		seed.transform.position = position;
		seed.rb2D.AddForce(velocity,ForceMode2D.Impulse);
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		var contact = collision.contacts[0];
		if (contact.normal.y > 0.95f) {
		
			Flower.Spawn(contact.point, null, preset);
			DestroyObject(gameObject, 0.01f);
		}
	}
}
