﻿using UnityEngine;
using System.Collections.Generic;

public class FlowerSpawner : MonoBehaviour {

	static float size = 1;
	[Range(1,100)]
	public int slots=1;

	List<int> freeSlots;
	Dictionary<Flower, int> usedSlots = new Dictionary<Flower, int>();

	public float spawnInterval=3f;
	private float spawnTimer;

	void Awake()
	{
		freeSlots = new List<int>(Mathf.Abs(slots));
		for (int i = 0; i < slots; i++) {
			freeSlots.Add(i);

		}

	}

	void Start()
	{
        spawnTimer = float.PositiveInfinity;
	}

	void Update()
	{
		spawnTimer += Time.deltaTime;

		if (spawnTimer> spawnInterval) {
			spawnTimer = 0;

			FlowerPresets preset = null;
			float presetDice = Random.Range(0, 1f);
			if (presetDice < 0.1f) {
				preset = Resources.Load<FlowerPresets>("Prefabs/FlowerTulipe");
			} else if (presetDice < 0.3f) {
				preset = Resources.Load<FlowerPresets>("Prefabs/FlowerRose");
			} else if (presetDice < 0.6f) {
				preset = Resources.Load<FlowerPresets>("Prefabs/FlowerMarguerite");
			} else if (presetDice < 0.9f) {
				preset = Resources.Load<FlowerPresets>("Prefabs/FlowerNenuphar");
			}

			TrySpawn(preset);
		}
	}

	public bool TrySpawn(FlowerPresets preset)
	{
		if (freeSlots.Count == 0) return false;

		int randSlot = Random.Range(0, freeSlots.Count);
		randSlot = freeSlots[randSlot];
		freeSlots.Remove(randSlot);

		//add flower spawn code
		var flower = Flower.Spawn(new Vector2(transform.position.x + 0.5f + randSlot * size, transform.position.y),
			this, preset);

		usedSlots.Add(flower, randSlot);

		return true;
	}

	public bool TryRemove(Flower flower)
	{
		int delSlot;
		if (usedSlots.TryGetValue(flower, out delSlot)){
			usedSlots.Remove(flower);
			freeSlots.Add(delSlot);

			Destroy(flower);
			return true;
		}

		return false;
	}

#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		var bottomLeft= new Vector3(transform.position.x, transform.position.y, 0);
		var bottomRight = new Vector3(transform.position.x+size*slots, transform.position.y, 0);
		var topLeft = new Vector3(transform.position.x, transform.position.y+ size, 0);
		var topRight = new Vector3(transform.position.x + size * slots, transform.position.y + size, 0);

		Gizmos.DrawLine(bottomLeft, bottomRight);
		Gizmos.DrawLine(bottomLeft, topLeft);
		Gizmos.DrawLine(topLeft, topRight);
		Gizmos.DrawLine(bottomRight, topRight);
		Gizmos.DrawLine(topLeft, bottomRight);
		Gizmos.DrawLine(bottomLeft, topRight);

	}
#endif
}
