﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "FlowerSprites",menuName = "FlowerPower/FlowerSprites",order=1)]
public class FlowerPresets : ScriptableObject {
	
	[SerializeField,Range(0,10)]
	private int _PointValue;
	public int PointValue { get { return _PointValue; } }

	public enum Type
	{
		Normal,
		Dash,
		PowerUp2,
		PowerUp3
	}

	public Type type;

	public Sprite icon;
	public Sprite[] spawning;
	public Sprite[] spawned;
	public Sprite[] cut;
	public Sprite[] onFloor;

	public ParticleSystem particles;

	public void Cut(BasePlayer player)
	{
		switch (type) {
			default: Debug.Log("Call Cut event"); break;
		}
	}


}
