﻿using UnityEngine;
using System.Collections;

public class RoadBandAnimation : MonoBehaviour {

	Material mat;
	public float speed;

	// Use this for initialization
	void Start () {
		mat = GetComponent<Renderer> ().material;
	}
	
	// Update is called once per frame
	void Update () {
		mat.SetTextureOffset( "_MainTex" ,new Vector2(mat.mainTextureOffset.x + (speed * Time.deltaTime),0));
	}
}
